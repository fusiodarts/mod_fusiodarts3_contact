<?php
/*------------------------------------------------------------------------
# mod_fusiodarts3_contact.php
# ------------------------------------------------------------------------
# version		3.0.0
# author    	Angel Albiach - Fusió d'Arts Technology S.L.
# copyright 	Copyright (c) 2016 Fusió d'Arts All rights reserved.
# @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website		http://www.fusiodarts.com
-------------------------------------------------------------------------
*/
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$version = new JVersion;
$jversion = '3';
if (version_compare($version->getShortVersion(), '3.0.0', '<')) {
    $jversion = '2.5';
}

$moduleId = $module->id;
        
// get style
$style = $params->get('styles', '');
if ($style == 0){
	$style_file = '1';
} else if($style == 1){
	$style_file = '2';
} else if($style == 2){
	$style_file = '3';
} else if($style == 3){
	$style_file = '4';
}

JHtml::stylesheet(JUri::base().'modules/mod_fusiodarts3_contact/assets/style'.$style_file.'.css', array(), true);
// add scripts
$doc = JFactory::getDocument();
$jquery= $params->get('jquery', '');
if($jquery == 1){
    if ($jversion == '3') {
        JHtml::_('jquery.framework', true);
    } else {
        $doc->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');
    }
}

//Email Parameters
$email_required = $params->get('email_required', 'true');
$recipient      = $params->get('email_recipient', '');
$fromEmail      = @$params->get('from_email', '');
$sendersname    = @$params->get('from_email_name', '');
$mySubject      = $params->get('email_subject', '');

//Phone Parameters
$phone_required = $params->get('phone_required', 'true');

// Text Parameters
$message_type   = $params->get('message_type', true);
$message_label  = $params->get('message_label', true);

// Captcha Parameters
$enable_anti_spam       = $params->get('enable_anti_spam', true);
$recaptcha_site_key     = $params->get('recaptcha_site_key', '');
$recaptcha_secret_key   = $params->get('recaptcha_secret_key', '');

// Thanks options
$email_thanks           = $params->get('email_thanks', true);
$email_thanks_subject   = $params->get('thanks_subject', '');
$email_thanks_message   = $params->get('email_thanks_message', '');

// Module Class Suffix Parameter
$mod_class_suffix = $params->get('moduleclass_sfx', '');

// Content
$enable_placeholder = $params->get('enable_placeholder', '');
$label_bootstrap    = $params->get('label_bootstrap', '');
$input_bootstrap    = $params->get('input_bootstrap', '');
$require_bootstrap  = $params->get('require_bootstrap', '');
$button_bootstrap   = $params->get('button_bootstrap', '');

$error_message  = '';
$valid_name     = '';
$valid_email    = '';
$valid_message  = '';
$valid_phone    = '';

require JModuleHelper::getLayoutPath('mod_fusiodarts3_contact', $params->get('layout', 'default'));