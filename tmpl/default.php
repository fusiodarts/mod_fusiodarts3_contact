<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');

$jinput = JFactory::getApplication()->input;

if ($jinput->get('name')) {

    $valid_name = '';    

    if ($jinput->get('email')) {
        $valid_email = '';
    }

    if ($jinput->get('phone')) {
        $valid_phone = '';
    }

    if ($jinput->get('message')) {
        $valid_message = '';
    }

    if ($enable_anti_spam) {
        if ($jinput->get('g-recaptcha-response')) {
            $captcha = $jinput->get('g-recaptcha-response');
        }
        $contextOpts = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false
            )
        );
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret='" . $recaptcha_secret_key . "'&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR'], false, stream_context_create($contextOpts));
        $result_captcha = json_decode($response, true);
        $result_captcha['success'];
        if ($result_captcha['success'] != 1) {
            header("Content-type: text/html; charset=utf-8");
            if (!count(array_diff(ob_list_handlers(), array('default output handler'))) || ob_get_length()) {
                while (@ob_end_clean());
            }
            echo JText::_('MOD_FUSIODARTS_CONTACT_WRONG_CAPTCHA');
            die();
            $error_message .= ' <span class="error-contact-form">' . JText::_('MOD_FUSIODARTS_CONTACT_WRONG_CAPTCHA') . '</span>';
        }
    }

    $current_url = JText::_('MOD_FUSIODARTS_CONTACT_SEND_FROM') . ' ' . JURI::current();
    $user_ip = JText::_('MOD_FUSIODARTS_CONTACT_IP') . ' ' . $_SERVER['REMOTE_ADDR'];
    jimport('joomla.environment.browser');
    $doc = JFactory::getDocument();
    $browser = JBrowser::getInstance();
    $browserType = JText::_('MOD_FUSIODARTS_CONTACT_BROWSER_TYPE') . ' ' . $browser->getBrowser();
    $browserVersion = JText::_('MOD_FUSIODARTS_CONTACT_BROWSER_VERSION') . ' ' . $browser->getMajor();
    $full_agent_string = JText::_('MOD_FUSIODARTS_CONTACT_FULL_AGENT_STRING') . ' ' . $browser->getAgentString();

    if ($email_required == 1) {
        if ($jinput->get('phone')) {
            $message_text = JText::_('MOD_FUSIODARTS_CONTACT_MESSAGE_INFO') . ' ' . $jinput->get('name') . ' - ' . $jinput->get('email') . "\n\n Phone: " . $jinput->get('phone') . "\n\n" . $jinput->get('message') . "\n\n" . $current_url . "\n\n" . $user_ip
                    . "\n\n" . $browserType . "\n\n" . $browserVersion . "\n\n" . $full_agent_string;
        } else {
            $message_text = JText::_('MOD_FUSIODARTS_CONTACT_MESSAGE_INFO') . ' ' . $jinput->get('name') . ' - ' . $jinput->get('email') . "\n\n" . $jinput->get('message') . "\n\n" . $current_url . "\n\n" . $user_ip
                    . "\n\n" . $browserType . "\n\n" . $browserVersion . "\n\n" . $full_agent_string;
        }
    } else {
        $message_text = "Name: " . $jinput->get('name') . " Phone: " . $jinput->get('phone') . "\n\n" . $jinput->get('message') . "\n\n" . $current_url . "\n\n" . $user_ip
                . "\n\n" . $browserType . "\n\n" . $browserVersion . "\n\n" . $full_agent_string;
    }

    // sending email to admin
    $mailSender = JFactory::getMailer();
    $mailSender->addRecipient($recipient);
    $mailSender->setSender(array($fromEmail, $jinput->get('name')));
    if ($email_required) {
        $mailSender->addReplyTo($jinput->get('email'), '');
    }
    $mailSender->setSubject($mySubject);
    $mailSender->setBody($message_text);
    if ($mailSender->Send()) {
        JFactory::getApplication()->enqueueMessage(JText::_('MOD_FUSIODARTS_CONTACT_SEND_OK'));
    } else {
        JFactory::getApplication()->enqueueMessage(JText::_('MOD_FUSIODARTS_CONTACT_SEND_ERROR'), 'error');
    }

    // sending thanks message
    if ($email_thanks && $email_required) {
        $mailSender_thanks = JFactory::getMailer();
        $mailSender_thanks->addRecipient($jinput->get('email'));

        $mailSender_thanks->setSender(array($fromEmail, $sendersname));
        $mailSender_thanks->addReplyTo($fromEmail, '');

        $mailSender_thanks->setSubject($email_thanks_subject);
        $mailSender_thanks->setBody($email_thanks_message . "\n\n" . JText::_('MOD_FUSIODARTS_CONTACT_YOUR_MESSAGE') . "\n" . $jinput->get('message'));

        $mailSender_thanks->Send();
    }
}

// check recipient
if ($recipient === "") {
    ?>
    <span class="error-contact-form"><?php echo JText::_('MOD_FUSIODARTS_CONTACT_NO_RECIPIENT'); ?></span>
    <?php return true;
}

$placeholder = '';
$enabled = 0;
?>

<div class="contact-form <?php echo $mod_class_suffix; ?>">
    <form class="easy-contact-form form-validate form-horizontal" action="<?php echo JFactory::getURI(); ?>" method="post">
        <?php if ($error_message != '') {
            print $error_message;
        } ?>
        
        <div class="form-group name">
            <div class="row">
                <?php // print name input 
                if ($enable_placeholder == 1) {
                    $enabled = 1;
                } else {
                    $placeholder = JText::_('MOD_FUSIODARTS_CONTACT_NAME_LABEL') . ' *';
                }

                if ($enabled == 1) { ?>
                    <div class="<?php echo $label_bootstrap; ?>">
                        <label>
                            <?php echo JText::_('MOD_FUSIODARTS_CONTACT_NAME_LABEL'); ?>
                            <span class="star">&nbsp;*</span>
                        </label>
                    </div>
                <?php } ?>

                <div class="<?php echo $input_bootstrap; ?>">
                    <input class="form-control inputbox required <?php echo $mod_class_suffix; ?>" type="text" name="name" id="name-<?php echo $moduleId; ?>" value="<?php echo $valid_name; ?>" required="required" placeholder="<?php echo $placeholder; ?>" />
                </div>
            </div>
        </div>
        
        <?php // print email input
        if ($email_required == 1) { ?>
            <div class="form-group email">
                <div class="row">
                    <?php if ($enable_placeholder == 1) {
                        $enabled = 1;
                    } else {
                        $placeholder = JText::_('MOD_FUSIODARTS_CONTACT_EMAIL_LABEL') . ' *';
                    }

                    if ($enabled == 1) { ?>
                        <div class="<?php echo $label_bootstrap; ?>">
                            <label>
                                <?php echo JText::_('MOD_FUSIODARTS_CONTACT_EMAIL_LABEL'); ?>
                                <span class="star">&nbsp;*</span>
                            </label>
                        </div>
                    <?php } ?>

                    <div class="<?php echo $input_bootstrap; ?>">
                        <input class="form-control inputbox required <?php echo $mod_class_suffix; ?>" type="email" name="email" id="email-<?php echo $moduleId; ?>" value="<?php echo $valid_email; ?>" required="required" placeholder="<?php echo $placeholder; ?>" />
                    </div>
                </div>
            </div>
        <?php } ?>
        
        <?php // print email input
        if ($phone_required == 1) { ?>
            <div class="form-group phone">
                <div class="row">
                    <?php if ($enable_placeholder == 1) {
                        $placeholder = '';
                        $enabled = 1;
                    } else {
                        $placeholder = JText::_('MOD_FUSIODARTS_CONTACT_PHONE_LABEL') . ' *';
                    }

                    if ($enabled == 1) { ?>
                        <div class="<?php echo $label_bootstrap; ?>">
                            <label>
                                <?php echo JText::_('MOD_FUSIODARTS_CONTACT_PHONE_LABEL'); ?>
                                <span class="star">&nbsp;*</span>
                            </label>
                        </div>
                    <?php } ?>

                    <div class="<?php echo $input_bootstrap; ?>">
                        <input class="form-control inputbox <?php echo $mod_class_suffix; ?>" type="phone" name="phone" id="email-<?php echo $moduleId; ?>" value="<?php echo $valid_phone; ?>" placeholder="<?php echo $placeholder; ?>" />
                    </div>
                </div>
            </div>
        <?php } ?>
        
        <?php // print message input ?>
        <div class="form-group message">
            <div class="row">
                <?php if ($enable_placeholder == 1) {
                    $placeholder = '';
                    $enabled = 1;
                } else {
                    $placeholder = $message_label . ' *';
                }
                
                if ($enabled == 1) { ?>
                    <div class="<?php echo $label_bootstrap; ?>">
                        <label>
                            <?php echo $message_label; ?>
                            <span class="star">&nbsp;*</span>
                        </label>
                    </div>
                <?php }

                if ($message_type == 1) { ?>
                    <div class="<?php echo $input_bootstrap; ?>">
                        <textarea class="form-control textarea required <?php echo $mod_class_suffix; ?>" name="message" id="message-<?php echo $moduleId; ?>" rows="4" cols="4" required="required" value="" placeholder="<?php echo $placeholder; ?>"><?php echo $valid_message; ?></textarea>
                    </div>
                <?php } else { ?>            
                    <div class="<?php echo $input_bootstrap; ?>">
                        <input class="form-control inputbox required <?php echo $mod_class_suffix; ?>" type="text" name="message" id="message-<?php echo $moduleId; ?>" value="<?php echo $valid_message; ?>" required="required" placeholder="<?php echo $placeholder; ?>" />
                    </div>
                <?php } ?>
            </div>
        </div>
        
        <?php //print anti-spam
        if ($enable_anti_spam && $recaptcha_site_key && $recaptcha_secret_key) { ?>
            <div class='form-group captcha-box'>
                <div class="row">
                    <div class='g-recaptcha' data-sitekey="<?php echo $recaptcha_site_key; ?>"></div>
                </div>
            </div>
        <?php } ?>

        <?php // print button ?>
        <div class="form-group button-box">
            <div class="row">
                <div class="<?php echo $require_bootstrap; ?>">
                    <span class="star">*&nbsp;</span>
                    <?php echo JText::_('REQUIRE'); ?>
                </div>
                <div class="<?php echo $button_bootstrap; ?>">
                    <input class="form-control button validate <?php echo $mod_class_suffix; ?> btn btn-info" type="submit" value="<?php echo JText::_('MOD_FUSIODARTS_CONTACT_BUTTON_LABEL'); ?>" />
                </div>
            </div>
        </div>
    </form>
</div>